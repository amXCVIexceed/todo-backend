exports.up = (pgm) => {
  pgm.addColumns('users', {
    email: { type: 'text', notNull: true, unique: true },
  });
};

exports.down = (pgm) => {
  pgm.dropColumns('users', 'email');
};
