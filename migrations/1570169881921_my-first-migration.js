exports.up = (pgm) => {
  pgm.createTable('tasks', {
    id: 'id',
    userid: { type: 'integer', notNull: false },
    contenttask: { type: 'varchar(250)' },
    state: { type: 'boolean' },
    created: { type: 'timestamp' },
    updated: { type: 'timestamp' },
  });

  pgm.createTable('users', {
    userid: 'id',
    firstname: { type: 'varchar(50)' },
    lastname: { type: 'varchar(50)' },
    password: { type: 'varchar(50)' },
    created: { type: 'timestamp' },
    updated: { type: 'timestamp' },
  });
};

exports.down = (pgm) => {
  pgm.dropTable('users');

  pgm.dropTable('tasks');
};
