exports.up = (pgm) => {
  pgm.createTable('recpass', {
    userid: 'id',
    resetPasswordToken: { type: 'text', notNull: false },
    resetPasswordExpires: { type: 'timestamp', notNull: false },
  });
};

exports.down = (pgm) => {
  pgm.dropTable('recpass');
};
