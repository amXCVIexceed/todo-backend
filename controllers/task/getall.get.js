const express = require('express');
const jwt = require('jsonwebtoken');
const client = require('../../db/index');
const auth = require('../../middleware/auth');

const router = express.Router();

router.get('/task/getall', auth, async (req, res) => {
  try {
    const tokenDecode = jwt.decode(req.query.token, process.env.SALT);
    const query = `SELECT * FROM tasks
                    WHERE userid = '${tokenDecode.userid}';`;
    const response = await client.query(query);
    if (!response.rows) {
      res.status(200).send("No tasks.");
    } else {
      res.send(response.rows);
    }
  } catch (err) {
    res.status(404).send(`ERROR: ${err.message}`);
  }
});

module.exports = router;
