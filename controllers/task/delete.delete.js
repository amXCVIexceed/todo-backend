const express = require('express');
const client = require('../../db/index');
const auth = require('../../middleware/auth');

const router = express.Router();

router.delete('/task/delete', auth, async (req, res) => {
  try {
    if (!req.body.id) {
      throw new SyntaxError("uninitialized id");
    }
    const query = ` DELETE FROM tasks 
                    WHERE id = '${req.body.id}' 
                    RETURNING * ;`;
    const response = await client.query(query);

    if (!response.rowCount) {
      throw new Error("no such id");
    }
    res.send(response.rows[0]);
  } catch (err) {
    if (err.name === 'SyntaxError') {
      res.status(412).send(`${err.message}`);
    } else {
      res.status(404).send("Not found. No such id.");
    }
  }
});

module.exports = router;
