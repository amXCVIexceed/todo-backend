const express = require('express');
const client = require('../../db/index');
const auth = require('../../middleware/auth');

const router = express.Router();

router.put('/task/put', auth, async (req, res) => {
  try {
    const query = `UPDATE tasks SET 
                        ${req.body.userid ? `userid = ${req.body.userid}, ` : ''}
                        ${req.body.contenttask ? `contenttask = '${req.body.contenttask}', ` : ''}
                        ${req.body.state ? `state = ${req.body.state}, ` : ''} WHERE id = 
                        ${req.body.id} RETURNING * ;`;
    const replaceComma = query.replace(/, {2}WHERE/g, ' WHERE ');
    const response = await client.query(replaceComma);
    res.send(response.rows[0]);
  } catch (err) {
    if (err.name === 'SyntaxError') {
      res.status(404).send(`${err.message}`);
    } else {
      res.status(404).send("Not found. No such id.");
    }
  }
});

module.exports = router;
