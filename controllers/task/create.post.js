const express = require('express');
const jwt = require('jsonwebtoken');
const client = require('../../db/index');
const auth = require('../../middleware/auth');

const router = express.Router();

router.post('/task/create', auth, async (req, res) => {
  try {
    const { token, userid, contenttask, state } = req.body;
    const tokenDecode = jwt.decode(token, process.env.SALT);

    const query = `INSERT INTO tasks 
                        (userid, contenttask, state, created)
                        VALUES (${tokenDecode.userid}, 
                                '${contenttask}', 
                                ${state},
                                now())
                        RETURNING * ;`;
    if (!userid || !contenttask || !state) {
      throw new SyntaxError("Error input data");
    }
    const response = await client.query(query);
    res.send(response.rows[0]);
  } catch (err) {
    res.status(404).send(`Error created: ${err.message}`);
  }
});

module.exports = router;
