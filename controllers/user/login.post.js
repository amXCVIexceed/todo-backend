const express = require('express');

const md5 = require('md5');
const jwt = require('jsonwebtoken');
const client = require('../../db/index');

const router = express.Router();

router.post('/user/login', async (req, res) => {
  try {
    const query = `SELECT * FROM users
                        WHERE email = '${req.body.email}'
                        AND password = '${md5(req.body.password + process.env.SALT)}';`;
    if (!req.body.email || !req.body.password) {
      throw new SyntaxError("Error input data");
    }
    const response = await client.query(query);
    if (!response.rows[0]) {
      res.status(401).send('The email or password you entered is incorrect');
    } else {
      const user = response.rows[0];
      res.json({
        type: true,
        data: user,
        token: jwt.sign(user, process.env.SALT),
      });
      res.send(response.rows[0]);
    }
  } catch (err) {
    res.send(err.message);
  }
});

module.exports = router;
