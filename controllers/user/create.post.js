const express = require('express');
const md5 = require('md5');
const client = require('../../db/index');

const router = express.Router();

router.post('/user/create', async (req, res) => {
  try {
    if (!req.body.firstname || !req.body.password) {
      throw new SyntaxError("Error input data");
    }
    const queryAdd = `INSERT INTO users 
                            (email, firstname, lastname, password, created)
                            VALUES ('${req.body.email}',
                            '${req.body.firstname}', 
                            '${req.body.lastname}',
                            '${md5(req.body.password + process.env.SALT)}',
                            now())
                      RETURNING * ;`;
    const response = await client.query(queryAdd);
    res.send(response.rows[0]);
  } catch (err) {
    res.status(404).send(`Failed to create new user. More details: ${err.message}`);
  }
});

module.exports = router;
