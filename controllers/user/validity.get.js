const express = require('express');
const jwt = require('jsonwebtoken');
const client = require('../../db/index');

const router = express.Router();

router.get('/user/validity', async (req, res) => {
  try {
    if (!req.query.token) {
      res.status(404).send('you are not authorized');
    }
    const tokenValidity = jwt.decode(req.query.token, process.env.SALT);
    const query = `SELECT * FROM users
                    WHERE email = '${tokenValidity.email}';`;
    const response = await client.query(query);

    if (!response.rows[0]) {
      res.status(404).send('you are not authorized');
    } else {
      res.send(response.rows[0]);
    }
  } catch (err) {
    res.status(404).send('you are not authorized');
  }
});

module.exports = router;
