const express = require('express');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const client = require('../../db/index');

const router = express.Router();

router.post('/user/recovery', async (req, res) => {
  const tokenDecode = jwt.decode(req.query.token, process.env.SALT);
  const query = `SELECT * FROM users
                 WHERE email = '${tokenDecode.email}';`;
  if (!tokenDecode.email) {
    throw new SyntaxError("Error input data");
  }
  const response = await client.query(query);

  if (!response.rows[0]) {
    res.status(401).send('The email you entered is incorrect');
  } else {
    const token = jwt.sign(response.rows[0], process.env.SALT);

    const queryForWrite = `INSERT into recpass
                        ("resetPasswordToken", "resetPasswordExpires")
                        values (
                        '${token}', now()   );`;
    await client.query(queryForWrite);

    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: `${process.env.EMAIL_ADDRESS}`,
        pass: `${process.env.EMAIL_PASSWORD}`,
      },
    });

    const mailOptions = {
      from: `${process.env.EMAIL_ADDRESS}`,
      to: `${tokenDecode.email}`,
      subject: 'Link To Reset Password',
      text:
          `You are receiving this because you (or someone else) have requested the reset of the password for your account.
          Please click on the following link, or paste this into your browser to complete the process within one hour of receiving it:
          ${process.env.URL}/resetpassword/?${token}
          If you did not request this, please ignore this email and your password will remain unchanged.`,
    };

    transporter.sendMail(mailOptions, (err, res) => {
      if (err) {
        res.status(404).send(`${err.message}`);
      } else {
        res.status(200).json('recovery email sent');
      }
    });
  }
});

router.get('/user/resetpassword', async (req, res) => {
  try {
    if (!req.query.token) {
      res.status(404).send('error!');
    }
    const tokenValidity = jwt.decode(req.query.token, process.env.SALT);
    const query = `SELECT * FROM recpass
                    WHERE "resetPasswordToken" = '${req.query.token}';`;
    const response = await client.query(query);
    if (!response.rows[0]) {
      res.status(404).send('invalid link!');
    } else {
      const queryForWrite = `UPDATE users SET
                        "password" = '${md5(req.query.password + process.env.SALT)}'
                        WHERE "userid" = ${tokenValidity.userid};`;
      await client.query(queryForWrite);
      res.send(`password changed! You password: ${req.query.password}`);
    }
  } catch (err) {
    res.status(404).send('error autorization!');
  }
});

module.exports = router;
