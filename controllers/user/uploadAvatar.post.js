const express = require('express');
const jwt = require('jsonwebtoken');
const { IncomingForm } = require('formidable');
const AWS = require('aws-sdk');
const fs = require('fs');
const client = require('../../db/index');

const s3 = new AWS.S3();
const router = express.Router();


router.post('/user/uploadavatar', async (req, res) => {
  try {
    if (!req.headers.authorization) {
      res.status(404).send('you are not authorized');
    }
    const tokenValidity = jwt.decode(req.headers.authorization, process.env.SALT);
    if (!req.body) throw new SyntaxError("Body not found");

    const form = new IncomingForm();
    form.uploadDir = process.env.AVATARS_PATH;
    form.on('file', async (field, file) => {
      const fileName = file.path.replace(`${process.env.AVATARS_PATH}/`, '');
      const path = `https://s3-us-west-2.amazonaws.com/${process.env.BUCKET_NAME}/${fileName}`;
      const fileContent = fs.readFileSync(file.path);

      await s3.putObject({
        ACL: 'public-read',
        Body: fileContent,
        Key: `${fileName}`,
        Bucket: process.env.BUCKET_NAME,
      }).promise();
      fs.unlinkSync(file.path);

      const query = `UPDATE users SET 
                    avatar = '${path}'
                    WHERE userid = ${tokenValidity.userid} ;`;
      await client.query(query);
      res.send(path);
    });
    form.parse(req);
  } catch (err) {
    res.status(404).send(err);
  }
});

module.exports = router;
