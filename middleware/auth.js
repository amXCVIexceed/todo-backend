const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  if (req.method === 'OPTIONS') next();

  const token = req.headers.authorization;

  if (token) {
    jwt.verify(token, process.env.SALT);
    next();
  } else {
    const err = new Error('No token provided!');
    err.status = 403;
    return next(err);
  }
};
