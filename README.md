# ReactJS TODO App

## Requirements

For development, you will only need Node.js and a node global package, npm, installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

###

## Install

    $ git clone https://gitlab.com/amXCVIexceed/todo-backend.git
    $ cd todo-backend
    $ npm install

## Configure app

Copy .example.env to .env then edit it with your settings. You will need:

  - database password 
  - database username 
  - server name
  - database name
  - random data that is used as an additional input to a one-way function that "hashes" a password
  - email address from which letters will be sent
  - password from email
  - URL

## Running the project

    $ nodemon bin/app.js

    The app embed for development a static connect server with livereload plugged. So each time you start the app, you get automatic refresh in the browser whenever you update a file.

## Simple build for production

$ npm build