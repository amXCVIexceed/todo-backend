/* eslint-disable global-require */
require('dotenv').config({ path: `${__dirname}/../.env` });
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const klawSync = require('klaw-sync');
const path = require('path');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({ origin: '*' }));


async function useControllers() {
  const paths = klawSync(`${__dirname}/../controllers`, { nodir: true });
  paths.forEach((file) => {
    if (path.basename(file.path)[0] === '_' || path.basename(file.path)[0] === '.') return;
    // eslint-disable-next-line import/no-dynamic-require
    app.use('/', require(file.path));
  });
}

useControllers();

const port = 1234;

app.listen(port, () => {
});
