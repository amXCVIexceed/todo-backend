module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "airbnb-base",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "no-control-regex": 0,
        "semi": ["error", "always"],
        "quotes": [0, "double"],
        "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
        "camelcase": 0,
        "no-underscore-dangle": 0,
        "no-restricted-syntax": 0,
        "no-await-in-loop": 0,
        "no-useless-escape": 0,
        "no-continue": 0,
        "no-param-reassign": 0,
        "no-plusplus": 0,
        "consistent-return": 0,
        "array-callback-return": 0,
        "guard-for-in": 0,
        "max-len": ["error", { "code": 150 }],
        "default-case": 0,
        "no-shadow": 0,
        "no-return-await": 0,
        "object-curly-newline": 0,
        "prefer-destructuring": ["error", {"object": true, "array": false}],
    }
};