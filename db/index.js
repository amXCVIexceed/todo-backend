const { Pool } = require('pg');

let pool;
let connectCount = 0;

const connect = async () => {
  if (!pool) {
    pool = new Pool({
      user: process.env.DB_USER,
      host: process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      password: process.env.DB_PASSWORD,
      port: 5432,
    });
  }

  pool.on('connect', () => {
    connectCount = 0;
  });
  pool.on('error', () => {
    if (connectCount < 5) {
      connectCount++;
      connect();
    }
  });
  return pool.connect();
};
module.exports = {
  query: async (query, values) => {
    const client = await connect();
    try {
      const result = await client.query(query, values);
      return result;
    } catch (err) {
      if ((!values || !values.length) && query.values && query.values.length) {
        values = query;
      }
      err.message = `Error in query:\n ${query.text ? query.text : query} ${values
        && values.length ? `, values: ${values}` : ''}\nerror message: ${err.message}`;
      throw err;
    } finally {
      client.release();
    }
  },
};
